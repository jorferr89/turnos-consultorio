package consultorio;
import consultorio.modelos.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( final String[] args) {
        Estado estado1 = new Estado("Estado1");
        TipoDocumento tipoDocumento1 = new TipoDocumento("TipoDocumento1");
        TipoTelefono tipoTelefono1 = new TipoTelefono("TipoTelefono");
        Barrio barrio1 = new Barrio("Barrio1");
        ObraSocial obraSocial1 = new ObraSocial("ObraSocial1");
        Sexo sexo1 = new Sexo("Sexo1");
        TipoPrestacion tipoPrestacion1 = new TipoPrestacion("TipoPrestacion1");
        Rol rol1 = new Rol("Rol1");
        System.out.println(estado1);
        System.out.println(tipoDocumento1);
        System.out.println(tipoTelefono1);
        System.out.println(barrio1);
        System.out.println(obraSocial1);
        System.out.println(sexo1);
        System.out.println(tipoPrestacion1);
        System.out.println(rol1);
    }
}
