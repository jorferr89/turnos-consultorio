package consultorio.modelos;

public class Barrio {

    private String nombre;

    public Barrio() {
    }

    public Barrio(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Barrio [nombre=" + nombre + "]";
    }

    
    
}
