package consultorio.modelos;

public class ObraSocial {

    private String nombre;

    public ObraSocial() {
    }

    public ObraSocial(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "ObraSocial [nombre=" + nombre + "]";
    }

    
    
}
