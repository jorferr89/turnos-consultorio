package consultorio.modelos;

public class TipoPrestacion {

    private String nombre;

    public TipoPrestacion() {
    }

    public TipoPrestacion(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "TipoPrestacion [nombre=" + nombre + "]";
    }

    
    
}
