package consultorio.modelos;

public class TipoTelefono {
    
    private String nombre;

    public TipoTelefono() {
    }

    public TipoTelefono(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "TipoTelefono [nombre=" + nombre + "]";
    }

    
}
